'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

module.exports = class FilterProvider extends ServiceProvider {
    register () {
        this.app.bind('AltPoint/Spleen/Exceptions/FilterException',
            () => require('../src/Exceptions/FilterException'))

        this.app.bind('AltPoint/Spleen/Filter',
            () => require('../src/SpleenFilter'))
    }

    boot () {

    }
}
