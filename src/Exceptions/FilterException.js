'use strict'

const { LogicalException } = require('@adonisjs/generic-exceptions')

module.exports = class FilterException extends LogicalException {
    code = 'FILTER_PARSING_EXCEPTION'
    status = 400
}
