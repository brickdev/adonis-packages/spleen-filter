'use strict'

const Database = use('Database')
const Logger = use('Logger')
const FilterException = use('AltPoint/Spleen/Exceptions/FilterException')
const { RuntimeException } = require('@adonisjs/lucid/src/Exceptions')
const { Clause, Target } = require('spleen')

const fieldByPath = path => {
    let target = `"${path.shift()}"`

    for (let i=0; i<path.length; i++) {
        target += (path.length-i > 1) ? '->' : '->>'
        target += Number.isInteger(path[i]) ? path[i] : `'${path[i]}'`
    }

    return target
}

const applyCondition = (builder, clause) => {
    const field = builder.RelatedModel
        ? Database.raw(`"${builder.RelatedModel.table}".${fieldByPath(clause.subject.path)}`)
        : Database.raw(fieldByPath(clause.subject.path))

    const apply = {
        eq: object => {
            builder.where(field, object)
        },
        neq: object => {
            builder.whereNot(field, object)
        },
        gt: object => {
            builder.where(field, '>', object)
        },
        gte: object => {
            builder.where(field, '>=', object)
        },
        lt: object => {
            builder.where(field, '<', object)
        },
        lte: object => {
            builder.where(field, '<=', object)
        },
        in: object => {
            builder.whereIn(field, object)
        },
        nin: object => {
            builder.whereNotIn(field, object)
        },
        between: object => {
            builder.whereBetween(field, [object.lower, object.upper])
        },
        nbetween: object => {
            builder.whereNotBetween(field, [object.lower, object.upper])
        },
        like: object => {
            builder.where(field, 'ilike', object.value.replace(/\*/g, '%'))
        },
        nlike: object => {
            builder.whereNot(field, 'ilike', object.value.replace(/\*/g, '%'))
        },
    }

    apply[clause.operator](clause.object)
}

const apply = (builder, clause) => {
    if (!(clause instanceof Clause)) {
        const message = 'Not a Clause instance'
        Logger.crit(message, clause)
        throw new FilterException(message)
    }

    if (!(clause.subject instanceof Target)) {
        const message = 'Not a Target instance'
        Logger.crit(message, clause.subject)
        throw new FilterException(message)
    }

    if (clause.subject.path.length === 0) {
        Logger.notice('No filter on clause', clause)
        return
    }

    let isNegative = false
    if (clause.subject.path[0].match(/^!/)) {
        isNegative = true
        clause.subject.path[0] = clause.subject.path[0]
            .replace(/^!/, '')
    }

    try {
        // Filter by relation
        const target = clause.subject.path[0]
        let additionalCondition = []

        if (clause.subject.path.length === 1) {
            switch (clause.operator.type) {
                case 'eq':
                    additionalCondition[0] = '='
                    break
                case 'neq':
                    additionalCondition[0] = '!='
                    break
                case 'lt':
                    additionalCondition[0] = '<'
                    break
                case 'lte':
                    additionalCondition[0] = '<='
                    break
                case 'gt':
                    additionalCondition[0] = '>'
                    break
                case 'gte':
                    additionalCondition[0] = '>='
                    break
            }

            if ([null, false, 0].includes(clause.object)) {
                additionalCondition[1] = "0"
            } else if (!Number.isNaN(clause.object)) {
                additionalCondition[1] = Number(clause.object)
            } else {
                additionalCondition = []
            }
            console.log(additionalCondition)
        }

        isNegative === false
            ? builder.whereHas(target, builder => {
                    clause.subject.path.shift()
                    apply(builder, clause)
                }, ...additionalCondition)
            : builder.whereDoesntHave(target, builder => {
                    clause.subject.path.shift()
                    apply(builder, clause)
                })
    } catch (error) {
        if (error instanceof RuntimeException) {
            switch (error.code) {
                // Filter by column
                case 'E_INVALID_MODEL_RELATION':
                    Logger.debug(error.message)
                    isNegative === true
                        ? builder.whereNot(builder => applyCondition(builder, clause))
                        : applyCondition(builder, clause)
                    return
            }
        }

        throw error
    }
}

module.exports = apply
