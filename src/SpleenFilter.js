'use strict'

const Logger = use('Logger')
const FilterException = use('AltPoint/Spleen/Exceptions/FilterException')
const { parse, Filter, Clause } = require('spleen')
const applyClause = require('./filter')

const getFilter = expression => {
    if (expression instanceof Filter) {
        return expression
    } else if (typeof(expression) === 'string') {
        Logger.debug(`Got filter expression: «${expression}»`)
        const filter = parse(expression)

        if (filter.success !== true) {
            throw new FilterException(filter.error.message)
        } else if (!(filter.value instanceof Filter)) {
            throw new FilterException(`Not a Filter instance got by .parse()`)
        }

        return filter.value
    }

    return null
}

const applyFilter = (builder, expression, conjunctive) => {
    const filter = getFilter(expression)
    if (!(filter instanceof Filter)) {
        Logger.notice("NO FILTER")
        return
    }

    const apply = builder => {
        filter.statements
            .forEach(statement => {
                if (statement.value instanceof Filter) {
                    applyFilter(builder, statement.value, statement.conjunctive)
                    return
                }

                if (!(statement.value instanceof Clause)) {
                    Logger.warning('Not a Clause instance', statement.value)
                    return
                }
            
                statement.conjunctive === 'or'
                    ? builder.orWhere(builder => applyClause(builder, statement.value))
                    : builder.andWhere(builder => applyClause(builder, statement.value))
            })
    }

    conjunctive === 'or'
        ? builder.orWhere(apply)
        : builder.andWhere(apply)
}

module.exports = applyFilter
