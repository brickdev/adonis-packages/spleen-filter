Spleen based filter for AdonisJS query builder
==============================================

# Назначение

Пакет реализует фильтрацию данных на базе [универсального диалекта Spleen](https://github.com/dsfields/spleen-node#readme).

# Установка

1. Добавить пакет в проект:
    ```bash
    adonis install https://gitlab.com/altpoint-services/adonis-packages/spleen-filter.git
    ```
2. Зарегистрировать сервис-провайдер в проекте. Для этого добавить в файл `start/app.js` в секцию `providers`:
   ```js
   const providers = [
        …

        '@alt-point/spleen-filter/providers/FilterProvider',
   ]
   ```

# Использование

В контроллере:

```js
const spleenFilter = use('AltPoint/Spleen/Filter')

module.exports = class Controller {
    async index ({ request }) {
        const filter = request.input('filter')
        const result = await Model.query()
            .where(builder => spleenFilter(builder, filter))
            .fetch()

        return result
    }
}
```

# Примеры использования фильтров и общая идеология

Пакет является универсальным и позволяет выполнять фильтрацию как по связанным сущностям, так и по отдельным полям JSON-документа.

### Пример со связями

Допустим, у нас есть _«Компания»_, в _«Компании»_ могут быть _«Пользователи»_, у каждого _«Пользователя»_ могут быть _«Документы»_. В контексте _кода_, это три модели:
`Company.users` → `User.documents` → `Document`
Соответственно, подразумевается что у моделей объявлены и _обратные_ связи:
`Document.user` → `User.company` → `Company`.

Создадим контроллер для работы с пользователями:
```js
const spleenFilter = use('AltPoint/Spleen/Filter')

module.exports = class UserController {
    async index ({ request }) {
        const filter = request.input('filter')
        const users = await Model.query()
            .where(builder => spleenFilter(builder, filter))
            .fetch()

        return users
    }
}
```

#### Примеры фильтров:

Здесь описаны разные значения для параметра `filter`

Показать всех пользователей, логин которых _начинается_ с «ivan»
`/login like "ivan*"`

Показать всех пользователей, у которых есть документы:
`/documents eq true`

Показать всех пользователей, у которых есть документы, созданные после _1 мая 2020 года_:
`/documents/created_at gte "2020-05-01"`

Показать всех пользователей, у которых есть более 10 документов, и организация называется «Рога и копыта»:
`/company/name eq "Рога и копыта" and /documents gt 10`

### JSON-поля

Иногда различную мета-информацию целесообразно хранить в JSON-поле БД.
Например, у пользователя поле `profile` типа `JSON`:
```json
"name": "Vasiliy Pupkin",
"passport": {
    "serial": "7500",
    "number": "123456",
    "issued_at": "2000-05-01"
}
```

#### Фильтрующие запросы при этом не будут отличаться от примеров со связями:

Все пользователи, с именем, начинающимся на «Vasiliy»:
`/profile/name like "vasiliy*"`

С паспортом выданным в 2000 году:
`/profile/passport/issued_by between ["2000-01-01", "2000-12-31"]`

### Исключающие фильтры

Иногда нужно фильтровать по «отрицательным» условиям.

Оторбазить пользователей, у которых **нет** документов, типа `invoice`:
`/!documents/type eq "invoice"`

Или, например, `/name neq "Vasya"` и `/!name eq "Vasya"` — это одно и то же :-)

### TODO

 * Реализовать поддержку других СУБД (сейчас только Postgres)
 * Искать вхождения по массивам в JSON-документах

# CREDITS

По всем вопросам можно писать [мне](https://t.me/VladisMus)
